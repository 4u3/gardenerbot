# -*- coding: utf-8 -*-

import sys
import config
import os
import telebot
from telebot import types
from datetime import datetime

bot = telebot.TeleBot(config.token)

os.system("mv last.log pre-last.log")




keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
keyboard.add(*[types.KeyboardButton(butt) for butt in ['/time', '/online','/info','/help','/changelog']])

@bot.message_handler(commands = ['start'])
def start(message):
    bot.send_message(message.chat.id, 'Привет!\nПолный список комманд: /help', reply_markup=keyboard)
    try:
        print(message.from_user.username + " использовал /start")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /start\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /start")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /start\n")
        f.close()



@bot.message_handler(commands = ['time'])
def time_give(message):
    now = datetime.strftime(datetime.now(), "%H:%M")
    now = 'В Новосибирске сейчас '+now
    bot.send_message(message.chat.id, now, reply_markup=keyboard)
    now = None
    try:
        print(message.from_user.username + " использовал /time")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /time\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /time")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /time\n")
        f.close()


@bot.message_handler(commands = ['online'])
def online_bot(message):
    bot.send_message(message.chat.id, "Да я тут! Что тебе нужно?\n Полный список комманд: /help", reply_markup=keyboard)
    try:
        print(message.from_user.username + " использовал /online")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /online\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /online")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /online\n")
        f.close()



@bot.message_handler(commands = ['info'])
def info(message):
    bot.send_message(message.chat.id, 'Этот бот служит для управления гидропонной системой. Сделан в 2017 году', reply_markup=keyboard)
    try:
        print(message.from_user.username + " использовал /info")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /info\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /info")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /info\n")
        f.close()


@bot.message_handler(commands = ['get_id'])
def id(message):
    bot.send_message(message.chat.id, message.chat.id, reply_markup=keyboard)
    try:
        print(message.from_user.username + " использовал /get_id")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /get_id\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /get_id")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /get_id\n")
        f.close()



@bot.message_handler(commands = ['help'])
def id(message):
    bot.send_message(message.chat.id, '/time - время в Новосибирске сейчас\n/online - проверка работы бота\n/info - для чего бот\n/changelog - изменения в обновлении\n', reply_markup=keyboard)
    try:
        print(message.from_user.username + " использовал /help")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /help\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /help")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /help\n")
        f.close()



@bot.message_handler(commands = ['version'])
def online_bot(message):
    bot.send_message(message.chat.id, "Prealpha 0.5", reply_markup=keyboard)
    try:
        print(message.from_user.username + " использовал /version")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /version\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /version")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /version\n")
        f.close()



@bot.message_handler(commands = ['changelog'])
def changes_from_file(message):
    f = open("changelog.txt",'r')
    changes = f.read()
    bot.send_message(message.chat.id,changes, reply_markup=keyboard)
    f = None
    changes = None
    try:
        print(message.from_user.username + " использовал /changelog")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /changelog\n")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /changelog")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /changelog\n")
        f.close()

@bot.message_handler(commands = ['quit'])
def close_bot(message):
    bot.send_message(message.chat.id,"Бот сейчас выключится.")
    try:
        print(message.from_user.username + " использовал /quit")
        f = open('last.log','a')
        f.write(message.from_user.username + " использовал /quit")
        f.close()
    except TypeError:
        print(message.from_user.first_name + " использовал /quit")
        f = open('last.log','a')
        f.write(message.from_user.first_name + " использовал /quit")
        f.close()
    a = 123 + "12vsdfsd" 
# Егор я незн как сделать проверку, как сделаешь напиши попытался использовать, типо фиг вам, а если id совпадает то использовал и надо придумать как без ошибки сделать quit.

    
if __name__ == '__main__':
    bot.polling(none_stop=True)
